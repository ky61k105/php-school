<?php
$options = getopt('u:', ['url:']);
$url = $options['u'] ??  $options['url'] ?? '';
$parsedUrl = parse_url($url);

$data = [
    'scheme' => $parsedUrl['scheme'],
    'host' => $parsedUrl['host'],
];

$path = $parsedUrl['path'] ?? '';
$query = $parsedUrl['query'] ?? '';

$data = array_merge($data, getDomainInfo($data['host']));

$resource = explode($data['host'], $url)[1] ?? '';
if ($resource) {
    $data['resource'] = $resource;
}

$hash = explode('#', $resource);
$hash = end($hash);
if ($hash) {
    $data['hash'] = $hash;
}

$extension_arr = explode('.', $path);
$extension = end($extension_arr);
if (count($extension_arr) > 1) {
    $data['extension'] = $extension;
}

if (isset($query)) {
  parse_str($query, $data['query']);
}

var_export($data);

function getDomainInfo($domain) {
    $domain = strtolower($domain);
    $arr = explode('.', $domain);

    $count = count($arr);
    if ($count <= 1 || $count  > 4) {
        throw new Error('Incorrect domain');
    }

    if ($count === 2) {
        return [
            'domain' => implode('.', $arr),
            'tld' => $arr['1'],
        ];
    }

    if ($count === 4) {
       $domain = $arr;
       unset($domain[0]);
       return [
           'subdomain' => $arr[0],
           'domain' => implode('.', $domain),
           'tld' => $arr['2'] . '.' . $arr['3'],
       ];
    }

    // $count === 3
    if ($arr[0] === 'www') {
        return [
            'subdomain' => 'www',
            'domain' => $arr['1'] . '.' . $arr['2'],
            'tld' => $arr['2'],
        ];
    }

    $tlds = ['org', 'net', 'aero', 'arpa', 'asia', 'biz', 'cat', 'com', 'coop', 'edu', 'gov', 'info', 'jobs', 'mil', 'mobi', 'museum', 'name', 'net', 'org', 'post', 'pro', 'tel', 'travel', 'xxx'];
    if (strlen($arr[2]) === 2 && in_array($arr[1], $tlds)) {
        return [
            'domain' => $domain,
            'tld' => $arr['1'] . '.' . $arr['2'],
        ];
    }

    return [
        'subdomain' => $arr['0'],
        'domain' => $arr['1'] . '.' . $arr['2'],
        'tld' => $arr['2'],
    ];
}
