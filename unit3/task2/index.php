<?php
$nominal_money_arr = [500, 200, 100, 50, 20, 10, 5, 2, 1];
$input = (int) ($argv[1] ?? 0);

$output = [];

$nominal_money_arr_length = count($nominal_money_arr);
for ($i = 0; $i < $nominal_money_arr_length; $i++) {
    $nominal = $nominal_money_arr[$i];
    $count = intdiv($input, $nominal);
    if ($count) {
        $output[$nominal] = $count;
        $input -= $count * $nominal;
    }
}

var_export($output);
