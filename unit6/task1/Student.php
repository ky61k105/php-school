<?php

class Student
{
    private $firstName;
    private $lastName;
    private $gender;
    private $status;
    private $gpa;

    const STATUSES = ['freshman', 'sophomore', 'junior', 'senior'];
    const MAX_GPA = 4;

    public function __construct(string $firstName, string $lastName, string $gender, string $status, float $gpa ) {
        if (!in_array($gender, ['male', 'female'])) {
            throw new Error('Incorrect gender');
        }

        if (!in_array($status, self::STATUSES)) {
            throw new Error('Incorrect status');
        }

        if ($gpa < 0 || $gpa > self::MAX_GPA) {
            throw new Error('Incorrect gpa');
        }

        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->gender = $gender;
        $this->status = $status;
        $this->gpa = $gpa;
    }

    public function showMyself(){
        echo json_encode(get_object_vars($this)), PHP_EOL;
    }
    public function studyTime(int $study_time) {
        $gpa = $this->gpa + log($study_time);
        $this->gpa = $gpa > self::MAX_GPA ? self::MAX_GPA : $gpa;
    }
}

$studentList = [
    ['firstName' => 'Mike', 'lastName' => 'Barnes', 'gender' => 'male', 'status' => 'freshman', 'gpa' => 4],
    ['firstName' => 'Jim', 'lastName' => 'Nickerson', 'gender' => 'male', 'status' => 'sophomore', 'gpa' => 3],
    ['firstName' => 'Jack', 'lastName' => 'Indabox', 'gender' => 'male', 'status' => 'junior', 'gpa' => 2.5],
    ['firstName' => 'Jane', 'lastName' => 'Miller', 'gender' => 'female', 'status' => 'senior', 'gpa' => 3.6],
    ['firstName' => 'Mary', 'lastName' => 'Scott', 'gender' => 'female', 'status' => 'senior', 'gpa' => 2.7],
];
$studyTimes = [60, 100, 40, 300, 1000];
$students = [];

forEach($studentList as $student) {
    $students[] = new Student(...array_values($student));
}

forEach($students as $student) {
    $student->showMyself();
}

echo PHP_EOL;

forEach($students as $index => $student) {
    $student->studyTime($studyTimes[$index]);
    $student->showMyself();
}

