<?php

class Stack
{
    private $items = [];
    private $length = 5;
    public function countItems() {
        return count($this->items);
    }
    public function isEmpty() {
        return $this->countItems() === 0;
    }

    public function push($element) {
        if ($this->countItems() + 1 > $this->length) {
            throw new Exception('Overflow error. Stack is full.');
        }
        $this->items[] = $element;
    }

    public function pop() {
        if($this->isEmpty()) {
            throw new Exception('Underflow error. Stack is empty.');
        }
        return array_pop($this->items);
    }
}
