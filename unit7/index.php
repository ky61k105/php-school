<?php

require "Stack.php";

function check($str) {
    $symbols = [
        'open' => [
            '[',
            '<',
            '{',
            '(',
        ],
        'close' => [
            ']',
            '>',
            '}',
            ')',
        ]
    ];
    $stack = new Stack();
    $str_arr = str_split($str);
    foreach ($str_arr as $symbol) {
        if(in_array($symbol, $symbols['open'])) {
            $stack->push($symbol);
            continue;
        }

        if(in_array($symbol, $symbols['close'])) {
            $index = array_search($symbol, $symbols['close']);
            $lastOpen = $stack->pop($symbol);
            if($lastOpen === $symbols['open'][$index]) {
                continue;
            }
            return false;
        }
    }
    return $stack->isEmpty();
}
try {
    var_export(check('(<{}>)'));
    echo PHP_EOL;
    var_export(check('(<{)'));
} catch (Throwable $e) {
    echo $e->getMessage();
}
