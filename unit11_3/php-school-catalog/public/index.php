<?php

require __DIR__ . '/../bootstrap/bootstrap.php';


$router = include __DIR__ . '/../routes/routes.php';

$app = new \App\Application($router);

echo $app->handleRequest(new \App\Http\Request());

