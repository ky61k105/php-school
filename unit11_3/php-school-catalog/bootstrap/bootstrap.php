<?php

ini_set('display_errors', true);

require_once __DIR__ . '/functions.php';


spl_autoload_register(function ($class) {
    $path = __DIR__ . '/php-school-catalog/' . str_replace('\\', '/', $class) . '.php';
    if (file_exists($path)) {
        require_once $path;
    }
});
