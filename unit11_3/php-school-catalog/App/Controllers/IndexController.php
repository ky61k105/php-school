<?php

namespace App\Controllers;

use App\Http\Request;


class IndexController
{
    public function index(Request $request) {
        return 'Hi Index';
    }
}
