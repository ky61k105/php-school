<?php

namespace App\Controllers;

use App\Http\Request;

class FormController
{
    public function view(Request $request) {
        return 'Form view';
    }

    public function index(Request $request) {
        return 'Form Index';
    }

    public function update(Request $request) {

    }

    public function delete(Request $request) {

    }

}
