<?php

namespace App;

use \App\Http\Request;

class Router
{
    protected $routes = [];

    public function add($method, $route, $controller, $action) {
        $this->routes[$method][$route] = [
            'controller' => $controller,
            'action' => $action,
        ];
    }
    public function resolve(Request $request) :array {
        $queryParams = $request->getQueryParams();

        $method = strtolower($request->getMethod());

        $route = empty($queryParams['r']) ? '/' : $queryParams['r'];

        if(!isset($this->routes[$method][$route])) {
            throw new \Exception('Error 404');
        }
        return $this->routes[$method][$route];
    }
}
