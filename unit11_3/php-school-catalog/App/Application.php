<?php

namespace App;

use App\Http\Request;

class Application {
    protected $request;
    protected $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function handleRequest(Request $request) {
        $data = $this->router->resolve($request);

        $controllerClass = $data['controller'];
        $controller = new $controllerClass();

        $action = $data['action'];
        return $controller->$action($request);
    }

}
