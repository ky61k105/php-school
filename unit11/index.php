<?php

require __DIR__ . '/vendor/autoload.php';

use \App\Automobile;
use \App\Car;
use \App\Truck;

$catalog = [
    Automobile::make(Car::class, 'Audi', 'a1', '2001', '1111111111','full'),
    Automobile::make(Car::class, 'Audi', 'a1', '2001', '1111111112','full'),
    Automobile::make(Car::class, 'Audi', 'a1', '2001', '1111111113','full'),
    Automobile::make(Car::class, 'Audi', 'a1', '2001', '1111111114','full'),
    Automobile::make(Car::class, 'Audi', 'a1', '2001', '1111111115','full'),
    Automobile::make(Truck::class, 'MAN', 'TGA 26.390', '2006', '1111111116', '1000t'),
];

print_r($catalog);
