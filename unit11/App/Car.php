<?php

namespace App;

class Car extends Automobile
{
    protected $equipment;

    public function __construct($equipment)
    {
        $this->setEquipment($equipment);
    }


    public function getEquipment()
    {
        return $this->equipment;
    }

    public function setEquipment(string $equipment): void
    {
        $this->equipment = $equipment;
    }
}
