<?php

namespace App;

class Automobile
{
    protected $marka;
    protected $model;
    protected $year;
    protected $vin;

    public static function make(string $type, string $marka, string $model, string $year, string $vin, string $argument){
        if (!class_exists($type)) {
            throw new \Error('Incorrect type of Auto');
        }
        $auto = new $type($argument);
        $auto->setMarka($marka);
        $auto->setModel($model);
        $auto->setYear($year);
        $auto->setVin($vin);
        return $auto;
    }

    public function getMarka()
    {
        return $this->marka;
    }
    public function setMarka(string $marka): void
    {
        $this->marka = $marka;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel(string $model): void
    {
        $this->model = $model;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function setYear(string $year): void
    {
        $this->year = $year;
    }

    public function getVin()
    {
        return $this->vin;
    }

    public function setVin(string $vin): void
    {
        $this->vin = $vin;
    }

}
