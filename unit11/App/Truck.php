<?php

namespace App;

class Truck extends Automobile
{
    protected $load_capacity;


    public function __construct($load_capacity)
    {
        $this->setLoadCapacity($load_capacity);
    }

    public function getLoadCapacity()
    {
        return $this->load_capacity;
    }

    public function setLoadCapacity($load_capacity): void
    {
        $this->load_capacity = $load_capacity;
    }
}
